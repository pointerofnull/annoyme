! Homepage: https://gitlab.com/pointerofnull/annoyme/raw/master/annoymehosts.txt
! Checksum: lp+2AtqdNecGyjNd6vYqhA
! Title: Annoyme Hosts
! License: https://creativecommons.org/publicdomain/zero/1.0/
! Version: 1.0

! Misc
|http://*.*.*.*/$subdocument,third-party,domain=anti-hacker-alliance.com
||adnxs.com^
||beanstock.com^
||beanstockmedia.com^
||betrad.com^
||boomtrain.com^
||btrll.com^
||data.quithappenbetting.com^
||duapps.com^
||fiftyone.com^
||flytomars.online^
||gigya.com^
||glispa.com^
||gvt1.com^
||instant-fix-online.club^
||onlinadverts.com^
||prniscr.pw^
||scanalert.com^
||screenshot-img.pw^
||smarterhq.io^
||verifying-system.com^
||video-ad-stats.googlesyndication.com^
||virtualrealitybros.com^
||x3.vindicosuite.com^
||xdating.com^

! SCAMS
||steampromotion.com^

! SPAM
||21-bestsex.blogspot.com^
||albertaimpaireddriving.com^
||arizonasfamilylawyer.com^
||best-sex.ml^
||cohenbestlaw.com^
||confessionsofahealthaholic.com^
||consumerguidetomosquitorepellants.com^
||diyiso.com^
||dtvo.com^
||dubsmashtelugu.com^
||facebook1111.com^
||fermery.com^
||ferrohomme.com^
||floridadockbuilders.com^
||focuslost.com^
||frankbeckett.com^
||free-csgo.com^
||get-an-affair.com^
||haveanaffairx.com^
||hawaiifive-orealty.com^
||heelsforhoofs.com^
||hookup9.com^
||instadatehookups.com^
||islcargo.com^
||jifone.com^
||johnxhetzel.com^
||mega-machine.com^
||merrychristmasclipart.com^
||mesoclaimattorney.com^
||mycoffeebay.com^
||nopainbk.com^
||onerichworld.com^
||pharmexs.com^
||pics9.net^
||readingtruck.com^
||russian-dating.cf^
||settings-google.com^
||sport-forecast.com^
||stemshare.com^
||tahitiwater.com^
||taotelaisi.com^
||taxcutpro.com^
||throat-spray.com^
||tiniday.com^
||tiredarguments.com^
||trailerd.com^
||travelfortheloveofwine.com^
||trendcars.net^
||txtrumpet.com^
||upprinters.com^
||vaibo.com^
||vegashdr.com^
||viralitypro.com^
||wakeflotshop.com^
||water4food.com^
||ways2shop.com^
||whitegloveparking.com^
||xiti.com^
||yardleyestatelawyer.com^
||yicairen.com^
||zmaxmovers.com^